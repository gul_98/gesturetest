package com.application.testapplication;

/**
 * Created by gul on 12/4/16.
 */
public interface PushListener {
    void onSuccess(Boolean status);
}
