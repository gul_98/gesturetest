package com.application.testapplication;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by gul on 12/3/16.
 */
public class SynchClass {
    private static SynchClass obj;
    private CopyOnWriteArrayList<Observer> observers;
    Context context;
    public SynchClass(Context context){
        this.context=context;
        observers=new CopyOnWriteArrayList<Observer>();
    }
    public SynchClass(){
        observers= new CopyOnWriteArrayList<Observer>();
    }
    public static SynchClass getInstance(Context context){
        if(obj==null){
            obj= new SynchClass(context);
        }
        return obj;
    }
    public void registerObserver(Observer observer){
        observers.add(observer);
    }
    public void processingFunction(){
        Log.e("Some Processing","1");
        //you will fetch contacts in this function and update local database and will notify observer

        Contact[] contacts= new Contact[2];
        contacts[0]= new Contact("Ravi", "9100000000");
        contacts[1]= new Contact("Srinivas", "9199999999");
        List<Object> list= new ArrayList<Object>();
        for(int i = 0 ; i< 2; i++){
           list.add(contacts[i]);

        }
        pushOfflineContatcs(list);
        List<Object> fetchedContacts= fetchOfflineContacts();
        for(int i = 0 ; i< fetchedContacts.size();i++){
            Log.e("Contact",((Contact)fetchedContacts.get(i))._name);
        }
        notifyObservers();
    }
    private synchronized void notifyObservers(){
        for(Observer ob : observers) {
           synchronized (ob) {
               ob.getNotified();
           }
        }
    }

    private List<Object> fetchOfflineContacts(){
        OfflineCommands command= new FetchOfflineContacts(context);
        return command.fetch();
    }
    private void pushOfflineContatcs(List<Object> contacts){
        OfflineCommands command = new PushOfflineContacts(context);
        command.push(contacts);
    }

}
