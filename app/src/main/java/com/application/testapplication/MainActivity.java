package com.application.testapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements Observer {
    private int testVariable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DatabaseHandler db= DatabaseHandler.getInstance(this);
        SynchClass.getInstance(this).registerObserver(this);
        SynchClass.getInstance(this).processingFunction();
        List<Object> list= new ArrayList<Object>();
        Log.e("notification","haha");
        Log.d("Insert: ", "Inserting ..");
       // db.addContact(new Contact("Ravi", "9100000000"));
        // db.addContact(new Contact("Srinivas", "9199999999"));
        //db.addContact(new Contact("Tommy", "9522222222"));
        //db.addContact(new Contact("Karthik", "9533333333"));
        testInterface test= new getAllContacts(db.getWritableDatabase(),list);
        test.execute();
        list=test.fetch();
       // Log.e("Size of list",String.valueOf(list.size()));
        //for(int i = 0 ; i < list.size();i++){
         //   Log.e("Contacts:",((Contact)(list.get(i)))._name);
        //}

    }

    @Override
    public void getNotified()

    {
        //load the array from the database
        // notify the adapter
        //Redoo the starting operations
        Log.e("yes i am notified","hhaha");
    }

}
