package com.application.testapplication;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gul on 12/4/16.
 */
public class FetchOfflineContacts extends OfflineCommands{
    private static final int DATABASE_VERSION = 1;


    // Database Name
    private static final String DATABASE_NAME = "contactsManager";

    // Contacts table name
    private static final String TABLE_CONTACTS = "contacts";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_PH_NO = "phone_number";

    @Override
    public void push(List<Object> contacts) {

    }

    @Override
    public List<Object> fetch() {
        SQLiteDatabase db= DBHandler.getInstance(context).getWritableDatabase();
        List<Object> contactList = new ArrayList<Object>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS;

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Contact contact = new Contact();
                contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setName(cursor.getString(1));
                contact.setPhoneNumber(cursor.getString(2));
                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }

        return contactList;
    }

    @Override
    public void addListener(PushListener listener) {
        super.addListener(listener);
    }

    FetchOfflineContacts(Context context) {
        super(context);
    }
}
