package com.application.testapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

/**
 * Created by gul on 12/4/16.
 */
public class PushOfflineContacts extends OfflineCommands{
    // Database Version
    private static final int DATABASE_VERSION = 1;


    // Database Name
    private static final String DATABASE_NAME = "contactsManager";

    // Contacts table name
    private static final String TABLE_CONTACTS = "contacts";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_PH_NO = "phone_number";

    @Override
    public void addListener(PushListener listener) {
        super.addListener(listener);
    }

    @Override
    public void push(List<Object> contacts) {
        for(int i = 0 ; i< contacts.size(); i++){
            Contact contact= (Contact)contacts.get(i);
            addContact(contact);
        }
    }

    @Override
    public List<Object> fetch() {
        return null;
    }

    PushOfflineContacts(Context context) {
        super(context);
    }

    private void addContact(Contact contact){

            SQLiteDatabase db = DBHandler.getInstance(context).getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_NAME, contact.getName()); // Contact Name
            values.put(KEY_PH_NO, contact.getPhoneNumber()); // Contact Phone

            // Inserting Row
            db.insert(TABLE_CONTACTS, null, values);
            db.close(); // Closing database connection
    }
}
