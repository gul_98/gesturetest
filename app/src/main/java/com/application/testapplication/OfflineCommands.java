package com.application.testapplication;

import android.content.Context;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by gul on 12/4/16.
 */
public abstract class OfflineCommands {
    CopyOnWriteArrayList<PushListener> listeners;
    Context context;
    OfflineCommands(Context context){
        this.context=context;
        listeners= new CopyOnWriteArrayList<PushListener>();
    }
    public void addListener(PushListener listener){
        listeners.add(listener);
    }
    public abstract List<Object> fetch();
    public abstract void push(List<Object> contacts);
}
