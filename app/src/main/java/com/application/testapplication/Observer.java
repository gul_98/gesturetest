package com.application.testapplication;

/**
 * Created by gul on 12/3/16.
 */
public interface Observer {
    void getNotified();
}
