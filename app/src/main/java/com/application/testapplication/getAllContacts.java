package com.application.testapplication;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.lang.Object;

/**
 * Created by gul on 12/2/16.
 */
public class getAllContacts implements testInterface{
    SQLiteDatabase db;
    List<Object> list;
    private static final String TABLE_CONTACTS = "contacts";
    public getAllContacts(SQLiteDatabase db,List<Object> list1){
        this.db=db;
        list=list1;
    }


    @Override
    public List<Object> fetch() {
        return list;
    }
    public Object noone(){
        return this;
    }

    public void execute(){
        Log.e("hello","hello");
        List<Object> contactList = new ArrayList<Object>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS;

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Contact contact = new Contact();
                contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setName(cursor.getString(1));
                contact.setPhoneNumber(cursor.getString(2));
                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }

        // return contact list
        Log.e("Contacts size",String.valueOf(contactList.size()));
        list=contactList;

    }

}