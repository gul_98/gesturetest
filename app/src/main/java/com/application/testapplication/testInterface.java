package com.application.testapplication;

import java.util.List;
import java.util.Objects;

/**
 * Created by gul on 12/2/16.
 */
public interface testInterface {
    void execute();
    List<Object> fetch();
}
